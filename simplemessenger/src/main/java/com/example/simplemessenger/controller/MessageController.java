package com.example.simplemessenger.controller;

import com.example.simplemessenger.model.Message;
import com.example.simplemessenger.service.MessagingService;
import jdk.incubator.http.HttpRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.net.URISyntaxException;

@RestController
public class MessageController {


    private static final String RECEIVE_PATH = "/receive";

    private static final String SEND_PATH = "/send";

    private static final String APPLICATION_JSON = "application/json";

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageController.class);

    private MessagingService service;



    @Autowired
    public MessageController(MessagingService service){

        this.service = service;
    }

    /**
     * Incoming messages from other messaging clients are received through this
     * endpoint and output to the console.
     *
     * @param request Message object detailing recipient, sender and message body
     * @param request The incoming HTTP request
     */
    @PostMapping(value = RECEIVE_PATH, consumes=APPLICATION_JSON)
    @ResponseStatus(HttpStatus.OK)
    public void receiveMessage(@RequestBody Message message, HttpServletRequest request){

        LOGGER.info("[FROM: " + message.getSender() + "]\n MESSAGE: " + message.getMessage());
    }

    /**
     * Messages sent to this endpoint are forwarded on to their intended recipient.
     *
     * @param message Message object detailing recipient, sender and message body
     * @return HTTP OK (if successful), HTTP BAD REQUEST (if not...)
     *
     * @throws URISyntaxException indicates that a string could not be parsed as a
     *                            URI reference.
     */
    @PostMapping(value = SEND_PATH, consumes=APPLICATION_JSON)
    //TODO: request body can only hold one object, change this method signature
    public ResponseEntity sendMessage(@RequestBody Message message) throws URISyntaxException {

        LOGGER.info("[TO " + message.getRecipient() + "] " + message.getMessage());

        HttpRequest request = service.buildRequest(message.getRecipient(), message.getMessage());
        boolean success = service.send(request);

        if(success){
            LOGGER.info("sent successfully");
            return ResponseEntity.ok().build();
        }

        LOGGER.info("failed to send");
        //technically send() may fail for other reasons, but we'll keep it simple and use HTTP 400
        return ResponseEntity.badRequest().build();
    }

}
