package com.example.simplemessenger.service;

import jdk.incubator.http.HttpClient;
import jdk.incubator.http.HttpRequest;
import jdk.incubator.http.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Service facilitates the construction and submission of HttpRequests
 *
 * @author simong 06/01/18
 */
@Service
public class MessagingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessagingService.class);

    public boolean send(HttpRequest request) {

        //HttpClient client = HttpClient.newHttpClient();
        HttpClient client = HttpClient.newHttpClient();

        try {
            HttpResponse<String> response =
                        client.send(request, HttpResponse.BodyHandler.discard(null));
            return true;

        } catch (IOException | InterruptedException e) {
            LOGGER.error("send: unable to send request ", e);
            return false;
        }
    }


    public HttpRequest buildRequest(String recipient, String requestBody) throws URISyntaxException {

        URI targetURI = new URI(recipient);

        return HttpRequest.newBuilder()
                .uri(targetURI)
                .header("content-type", "application/json")
                .POST(HttpRequest.BodyProcessor.fromString(requestBody))
                .build();
    }
}
