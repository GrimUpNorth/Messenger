package com.example.simplemessenger.model;

/**
 * Simple model class representing a message incl. who sent it and where it is going.
 */
public class Message {

    /** Intended recipient of this message */
    private String recipient;

    /** Identifies the sender of this message */
    private String sender;

    /** The message body of this message */
    private String message;

    public Message(){}

    public Message(String recipient, String sender, String message){

        this.recipient = recipient;
        this.sender = sender;
        this.message = message;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
