package com.example.simplemessenger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@EnableAutoConfiguration
@SpringBootApplication
@ComponentScan("com.example.simplemessenger")
public class SimpleMessengerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleMessengerApplication.class, args);
	}
}
