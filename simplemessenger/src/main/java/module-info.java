open module com.example.simplemessenger {
    requires spring.context;
    requires slf4j.api;
    requires jdk.incubator.httpclient;
    requires spring.web;
    requires spring.beans;
    requires gson;
    requires tomcat.embed.core;
    requires spring.boot.autoconfigure;
    requires spring.boot;
    requires java.sql;

    exports com.example.simplemessenger.controller;
    exports com.example.simplemessenger.service;
    exports com.example.simplemessenger;
}